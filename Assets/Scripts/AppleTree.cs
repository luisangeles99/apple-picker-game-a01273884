﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleTree : MonoBehaviour
{

    [Header("Set in Inspector")]
    public GameObject applePrefab;// Prefab for instantiating apples

    public float speed = 10f;// Speed at which the AppleTree moves
    public float leftAndRightEdge = 10f;// Distance where AppleTree turns around
    public float chanceToChangeDirections = 0.02f;// Chance that the AppleTree will change directions
    public float secondsBetweenAppleDrops = 1f;// Rate at which Apples will be instantiated


    // Start is called before the first frame update
    void Start()
    {
        Invoke("DropApple", 2f);
    }

    void DropApple()
    {                                                
        GameObject apple = Instantiate<GameObject>(applePrefab);   
        apple.transform.position = transform.position;                
        Invoke("DropApple", secondsBetweenAppleDrops);               
    }


    // Update is called once per frame
    void Update()
    {
        //Movimiento
        Vector3 pos = transform.position;
        pos.x += speed * Time.deltaTime;
        transform.position = pos;


        //Cambiar de direccion
        if (pos.x < -leftAndRightEdge){
            speed = Mathf.Abs(speed);
        }
        else if (pos.x > leftAndRightEdge) {
            speed = -Mathf.Abs(speed);
        }
        
    }

    void FixedUpdate() //Se ejecuta 50 veces por segundo
    {
        if (Random.value < chanceToChangeDirections)
        {
            speed *= -1;
        }
    }
}
