﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Basket : MonoBehaviour
{
    [Header("Set Dinamically")]
    public Text scoreGT;

    // Start is called before the first frame update
    void Start()
    {
        GameObject scoreGO = GameObject.Find("ScoreCounter");
        scoreGT = scoreGO.GetComponent<Text>();
        scoreGT.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        //Obtener posicion del mouse
        Vector3 mousePos2D = Input.mousePosition;

        // The Camera's z position sets how far to push the mouse into 3D
        mousePos2D.z = -Camera.main.transform.position.z;

        // Convertir espacio de pantalla 2d a 3d
        Vector3 mousePos3D = Camera.main.ScreenToWorldPoint(mousePos2D);


        //Mover posicion x de la canasta con la posicion x del mouse
        Vector3 pos = this.transform.position;

        pos.x = mousePos3D.x;

        this.transform.position = pos;
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject coll = collision.gameObject;
        if (coll.tag == "Apple") {
            Destroy(coll);

            int score = int.Parse(scoreGT.text);
            score += 100;
            scoreGT.text = score.ToString();

            if (score > HighScore.score){
                HighScore.score = score;
            }
        }
    }
}
